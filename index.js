//Задание 1

typeof(9)
// Предположение: Number
// Фактический: Number

typeof(1.2)
// Предположение: Number
// Фактический: Number

typeof(NaN)
// Предположение: Number
// Фактический: Number

typeof("Hello World")
// Предположение: String
// Фактический: String

typeof(true)
// Предположение: Boolean
// Фактический: Boolean

typeof(2 != 1)
// Предположение: Boolean
// Фактический: Boolean


"сыр" + "ы"
// Предположение: сыры
// Фактический: сыры

"сыр" - "ы"
// Предположение: ср
// Фактический: NaN

"2" + "4"
// Предположение: 24
// Фактический: 24

"2" - "4"
// Предположение: -2
// Фактический: -2

"Сэм" + 5
// Предположение: Сэм5
// Фактический: Сэм5

"Сэм" - 5
// Предположение: Error
// Фактический: NaN

99 * "шары"
// Предположение: Error
// Фактический: NaN

//Задание 2

let rectangle = {
    length: 37,
    width: 14,
    perimeter: 0,
    square: 0,
    ratio: 0
};

console.log('Длина прямоугольника: ', rectangle.length, ', ширина прямоугольника: ', rectangle.width);
rectangle.perimeter = (rectangle.length + rectangle.width) * 2;
console.log('Перимертр прямоугольника: ', rectangle.perimeter);
rectangle.square = rectangle.length * rectangle.width;
console.log('Площадь прямоугольника: ', rectangle.square);
rectangle.ratio = rectangle.perimeter / rectangle.square;
console.log('Отношение периметра к площади прямоугольника: ', rectangle.ratio);

// Задание 3

let temperatureOne = {
    celsius: 36.6,
    fahrenheit: 0
};

let temperatureTwo = {
    celsius: 0,
    fahrenheit: 36.6
};

temperatureOne.fahrenheit = temperatureOne.celsius * 9 / 5 + 32;
temperatureTwo.celsius = (temperatureTwo.fahrenheit -32) * 5 / 9;

console.log(temperatureOne.celsius.toFixed(1), '\xB0C соответствует ', temperatureOne.fahrenheit.toFixed(1), '\xB0F');
console.log(temperatureTwo.fahrenheit.toFixed(1), '\xB0F соответствует ', temperatureTwo.celsius.toFixed(1), '\xB0C');


// Задание 4

let year = prompt('Введите год, чтобы узнать високосный ли он:');

alert(year%4 == 0? (year%100 == 0? (year%400 == 0? 'Високосный' : 'Не является високосным') : 'Не является високосным') : 'Не является високосным');

// Задание 5

let numberTen = {
    one: 2,
    two: 8
};

console.log(numberTen.one == 10 || numberTen.two == 10 || numberTen.one + numberTen.two == 10);


// Задание 6


let amountOfSheeps = prompt('Введите количество овец:');
let lullaby = "";
let i = 1;

while (i <= amountOfSheeps) {
    lullaby += i + " овечка...";
    i++;
};

console.log(lullaby);

// Задание 7

for (let i = 0; i <=15 ; i++) {
    console.log(i%2 == 0? i + ' чётное' : i + ' нечётное');
};

// Задание 8

let christmasTree = "";
let symbolOfCt = "";

for (let i = 1; i <=12 ; i++) {
    i%2 == 1 ? symbolOfCt = "*" : symbolOfCt = "#";
    for (let k = 1; k <=i ; k++) {
        christmasTree += symbolOfCt;
    };
    christmasTree += "\n";
};

console.log(christmasTree);

// Задание 9

let numberSort = {
    one: 0,
    two: -3,
    three: 1
};

if (numberSort.one <= numberSort.two && numberSort.one <= numberSort.three) {
    if (numberSort.two <= numberSort.three) {
        console.log(numberSort.one, numberSort.two, numberSort.three);
    } else {
        console.log(numberSort.one, numberSort.three, numberSort.two);
    };
} else if (numberSort.two <= numberSort.one && numberSort.two <= numberSort.three) {
    if (numberSort.one <= numberSort.three) {
        console.log(numberSort.two, numberSort.one, numberSort.three);
    } else {
        console.log(numberSort.two, numberSort.three, numberSort.one);
    };
} else {
    if (numberSort.two <= numberSort.one) {
        console.log(numberSort.three, numberSort.two, numberSort.one);
    } else {
        console.log(numberSort.three, numberSort.one, numberSort.two);
    };
};

// Задание 10

let numberMax = {
    one: 2,
    two: -1,
    three: 0,
    four: -5,
    five: -4
};

switch (Math.max(numberMax.one, numberMax.two, numberMax.three, numberMax.four, numberMax.five)) {
    case numberMax.one: 
        console.log(numberMax.one);
        break;
    case numberMax.two :
        console.log(numberMax.two);
        break;
    case numberMax.three :
        console.log(numberMax.three);
        break;
    case numberMax.four :
        console.log(numberMax.four);
        break;
    case numberMax.five :
        console.log(numberMax.five);
}